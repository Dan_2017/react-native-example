import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  View,
  Text,
  Dimensions,
} from 'react-native';
import MapView from 'react-native-maps';
import flagBlueImg from './assets/flag-blue.png';
import flagPinkImg from './assets/flag-pink.png';
import flagRedImg from './assets/flag-red.png';
import flagGreenImg from './assets/flag-green.png';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

class MarkerTypes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentMarker: 0,
      marker: [
        true,
        false,
        false,
        false,
        false,
      ],
      activeItem: 0,
    };
  }

  handleScroll(e) {
    let x = e.nativeEvent.contentOffset.x;
    let p = (x - x%260)/260;
    if(this.state.currentMarker != p) {
      let markers = [ ...this.state.marker ];
      for(var i = 0; i < markers.length; i++) {
          if (i == p) {
            console.log("[ACTIVE]", p);
            markers[i] = true;
          }
          else markers[i] = false;
      }
      this.setState({ currentMarker: p});
      this.setState({ marker: markers });
    }
    console.log(p);
  }

  setMarker(n) {
    let key = "marker"+n;
    return this.state.marker[n-1] ? flagRedImg : flagGreenImg;
  }

  toggleMarker(n) {
    let markers = [ ...this.state.marker ];
    markers[n-1] = !markers[n-1];
    this.setState({ markers });
  }

  toggleContentView(n) {
    this.setState({ activeItem: this.state.activeItem? 0 : n });
  }

  setContentStyle() {
    return this.state.activeItem ? styles.contentContainer2 : styles.contentContainer;
  }

  setContentScroll() {
    return this.state.activeItem ? false : true;
  }

  setDescriptionStyle() {
    return this.state.activeItem ? styles.itemDescr : styles.displayNone;
  }

  setItemImgStyle() {
    return this.state.activeItem ? styles.itemImgOpen : styles.itemImg;
  }

  setItemStyle(n) {
    if(this.state.activeItem){
      if(n == this.state.activeItem) return styles.itemLinkOpen;
      else return styles.displayNone;
    } else {
      return styles.itemLink;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          provider={this.props.provider}
          style={styles.map}
          initialRegion={{
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}
        >
          <MapView.Marker
            onPress={() => {this.toggleMarker(1)}}
            coordinate={{
              latitude: LATITUDE + SPACE,
              longitude: LONGITUDE + SPACE,
            }}
            centerOffset={{ x: 0, y: 0 }}
            anchor={{ x: 0.69, y: 1 }}
            image={this.setMarker(1)}
          >
            <Text style={styles.marker}>10$</Text>
          </MapView.Marker>
          <MapView.Marker
            onPress={() => {this.toggleMarker(2)}}
            coordinate={{
              latitude: LATITUDE - SPACE,
              longitude: LONGITUDE - SPACE,
            }}
            centerOffset={{ x: 0, y: 0 }}
            anchor={{ x: 0.69, y: 1 }}
            image={this.setMarker(2)}
          >
            <Text style={styles.marker}>20$</Text>
          </MapView.Marker>
          <MapView.Marker
            onPress={() => {this.toggleMarker(3)}}
            coordinate={{
              latitude: LATITUDE - SPACE,
              longitude: LONGITUDE + SPACE,
            }}
            centerOffset={{ x: 0, y: 0 }}
            anchor={{ x: 0.69, y: 1 }}
            image={this.setMarker(3)}
          >
            <Text style={styles.marker}>15$</Text>
          </MapView.Marker>
          <MapView.Marker
            onPress={() => {this.toggleMarker(4)}}
            coordinate={{
              latitude: LATITUDE + SPACE,
              longitude: LONGITUDE - SPACE,
            }}
            centerOffset={{ x: 0, y: 0 }}
            anchor={{ x: 0.69, y: 1 }}
            image={this.setMarker(4)}
          >
            <Text style={styles.marker}>15$</Text>
          </MapView.Marker>
          <MapView.Marker
            onPress={() => {this.toggleMarker(5)}}
            coordinate={{
              latitude: LATITUDE,
              longitude: LONGITUDE,
            }}
            centerOffset={{ x: 0, y: 0 }}
            anchor={{ x: 0.69, y: 1 }}
            image={this.setMarker(5)}
          >
            <Text style={styles.marker}>15$</Text>
          </MapView.Marker>
        </MapView>
        <ScrollView style={styles.map} contentContainerStyle={this.setContentStyle()} horizontal={this.setContentScroll()} onScroll={(e)=>{this.handleScroll(e)}} scrollEventThrottle={1}>
          <View style={this.setItemStyle(1)}>
            <TouchableOpacity style={styles.touchArea} onPress={()=>{this.toggleContentView(1)}}>
              <Image
                source = {require('./assets/rooms/1.jpg')}
                style = {this.setItemImgStyle()}
              />
            </TouchableOpacity>
          </View>
          <View style={this.setItemStyle(2)}>
            <TouchableOpacity style={styles.touchArea} onPress={()=>{this.toggleContentView(2)}}>
              <Image
                source = {require('./assets/rooms/2.jpg')}
                style = {this.setItemImgStyle()}
              />
            </TouchableOpacity>
          </View>
          <View style={this.setItemStyle(3)}>
            <TouchableOpacity style={styles.touchArea} onPress={()=>{this.toggleContentView(3)}}>
              <Image
                source = {require('./assets/rooms/3.jpg')}
                style = {this.setItemImgStyle()}
              />
            </TouchableOpacity>
          </View>
          <View style={this.setItemStyle(4)}>
            <TouchableOpacity style={styles.touchArea} onPress={()=>{this.toggleContentView(4)}}>
              <Image
                source = {require('./assets/rooms/4.jpg')}
                style = {this.setItemImgStyle()}
              />
            </TouchableOpacity>
          </View>
          <View style={this.setItemStyle(5)}>
            <TouchableOpacity style={styles.touchArea} onPress={()=>{this.toggleContentView(5)}}>
              <Image
                source = {require('./assets/rooms/5.jpg')}
                style = {this.setItemImgStyle()}
              />
            </TouchableOpacity>
          </View>
          <View style={this.setDescriptionStyle()}>
            <Text style={styles.itemDescrHeader}>Room #{this.state.activeItem}</Text>
            <Text style={styles.itemDescrSubtitle}>Posted by Avi</Text>
            <Text style={styles.itemDescrText}>Lorem ipsum dolor set amet Lorem ipsum dolor set amet Lorem ipsum dolor set amet Lorem ipsum dolor set amet Lorem ipsum dolor set amet Lorem ipsum dolor set amet Lorem ipsum dolor set amet </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

MarkerTypes.propTypes = {
  provider: MapView.ProviderPropType,
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  marker: {
    marginLeft: 18,
    marginTop: 16,
    fontWeight: 'bold',
    color: '#fff',
    textShadowColor: '#000',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 3
  },
  contentContainer: {
    top: "75%",
    left: "0%",
    height: "25%",
    // width: '100%',
    backgroundColor: '#fff',
    shadowOffset:{  width: 0,  height: 0 },
    shadowColor: 'black',
    shadowOpacity: .15,
    shadowRadius: 7,
    borderTopWidth: 1,
    borderTopColor: "#eee",
  },
  contentContainer2: {
    top: "0%",
    left: "0%",
    backgroundColor: '#fff',
    minHeight: "100%",
    width: '100%',
  },
  touchArea: {
    backgroundColor: "#eee",
    // height: "100%",
  },
  itemLink: {
    shadowOffset:{  width: 2,  height: 5 },
    shadowColor: 'black',
    shadowOpacity: .75,
    shadowRadius: 7,
    backgroundColor: '#eee',
    width: 280,
    height: 128,
    margin: 20,
  },
  itemLinkOpen: {
    shadowOffset:{  width: 2,  height: 5 },
    shadowColor: 'black',
    shadowOpacity: .75,
    shadowRadius: 7,
    backgroundColor: '#eee',
    width: "100%",
    height: "40%",
    margin: 0,
  },
  itemImg: {
    width: 280,
    height: 128,
  },
  itemImgOpen: {
    width: "100%",
    height: "100%",
  },
  displayNone: {
    display: "none",
  },
  itemDescr: {
    width: '100%',
  },
  itemDescrHeader: {
    margin: 20,
    marginTop: 40,
    fontSize: 36,
    fontWeight: "bold",
  },
  itemDescrSubtitle: {
    margin: 0,
    marginLeft: 20,
    marginRight: 20,
    fontSize: 25,
  },
  itemDescrText: {
    margin: 20,
    fontSize: 18,
  },
});

module.exports = MarkerTypes;
